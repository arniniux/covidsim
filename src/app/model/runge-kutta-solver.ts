/**
 * Runge Kutta ODE Solver, Copyright (c) 2020 by Markus Schwehm, ExploSYS GmbH
 */

export interface Dgl {
  set(key: string, value: number): void;
  init(params: object): number[];
  eval(x: number, y: number[], dy: number[]): void;
}

export class RungeKuttaSolver {

  dgl: Dgl;
  x: number;
  y: number[];
  n: number;

  yy: number[];
  k1: number[];
  k2: number[];
  k3: number[];
  k4: number[];

  h: number;
  evals: number;
  steps: number;

  maxError: number;
  minError: number;

  constructor() {
    this.error(0.02);
  }

  public init( dgl: Dgl, x: number, y: number[] ) {

    this.dgl = dgl;
    this.x = x;
    this.y = y;

    this.n = y.length;
    this.yy = new Array<number>(this.n);
    this.k1 = new Array<number>(this.n);
    this.k2 = new Array<number>(this.n);
    this.k3 = new Array<number>(this.n);
    this.k4 = new Array<number>(this.n);

    this.h = 1;
    this.evals = 0;
    this.steps = 0;
  }

  public error( error: number ) {
    this.maxError = error;
    this.minError = error / 4;
  }

  public run( goal: number ) {
    while ( this.x + this.h < goal ) { this.step(); }

    const hOld = this.h;

    while ( Math.abs((this.x - goal) / goal) > 1E-13 ) {
      this.h = Math.min( goal - this.x, this.h );
      this.step();
    }

    this.h = hOld;
  }

  public getY(): number[] {
    return this.y;
  }

  public getSteps(): number {
    return this.steps;
  }

  public getEvals(): number {
    return this.evals;
  }

  private step() {
    this.dgl.eval( this.x, this.y, this.k1 );
    // console.log(this.x, this.y, this.k1);
    const half = 0.5 * this.h;
    for ( let i = this.n; i--; ) { this.yy[i] = this.y[i] + half * this.k1[i]; }
    this.dgl.eval( this.x + half, this.yy, this.k2 );
    // console.log(this.x + half, this.yy, this.k2);
    for ( let i = this.n; i--; ) { this.yy[i] = this.y[i] + half * this.k2[i]; }
    this.dgl.eval( this.x + half, this.yy, this.k3 );
    // console.log(this.x + half, this.yy, this.k3);
    this.evals += 3;
    let normK = 0;
    let max = 0;
    for ( let i = this.n; i--; ) {
      const diffK1K2 = this.k1[i] - this.k2[i];
      normK += diffK1K2 * diffK1K2;
      max = Math.max(max, Math.abs( this.k2[i] - this.k3[i] ));
    }
    let error = this.minError;
    if (normK > 0) { error = max / Math.sqrt(normK); }
    if ( (error > this.maxError) && ( this.h > 0.01 ) ) { this.h /= 2; return; }
    for ( let i = this.n; i--; ) { this.yy[i] = this.y[i] + this.h * this.k3[i]; }
    this.dgl.eval( this.x + this.h, this.yy, this.k4 );
    // console.log(this.x + this.h, this.yy, this.k4);
    this.evals ++;
    this.x += this.h;
    const h6 = this.h / 6;
    for ( let i = this.n; i--; ) {
      this.y[i] += h6 * ( this.k1[i] + 2 * (this.k2[i] + this.k3[i]) + this.k4[i]);
    }
    // console.log(this.x, this.y);
    if ( error < this.minError ) { this.h *= 2; }
    this.steps++;
  }

}
