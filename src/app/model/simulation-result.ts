export interface Dataset {
  values: { x: number, y: number }[];
  key: string;
  label: string;
  color?: string;
  update?: number;
  series?: string;
  value?: number;
}

export interface SimulationResult {
  popsize: Dataset;

  susceptible: Dataset;
  infected: Dataset;
  recovered: Dataset;
  dead: Dataset;
  detection: Dataset;

  r0: Dataset;

  latent: Dataset;
  prodromal: Dataset;
  asymptomatic: Dataset;
  sick: Dataset;
  hospitalized: Dataset;
  icu: Dataset;
  isolated: Dataset;
  secluded: Dataset;
  contactReduction: Dataset;

  CIinfections: Dataset;
  CIsick: Dataset;
  CIoutpatients: Dataset;
  CIhospitalisations: Dataset;
  CIicu: Dataset;
  CIdeaths: Dataset;

  DIinfections: Dataset;
  DIsick: Dataset;
  DIoutpatients: Dataset;
  DIhospitalisations: Dataset;
  DIicu: Dataset;
  DIdeaths: Dataset;

  WIinfections: Dataset;
  WIsick: Dataset;
  WIoutpatients: Dataset;
  WIhospitalisations: Dataset;
  WIicu: Dataset;
  WIdeaths: Dataset;

  update: number;
}
